#!/bin/sh
# Generate the content of a meson-cross.txt file for usage to cross-compile
# against this chroot. Should be run once the chroot is in its final location
# to make sure all the patch are right

# Assumes that a cross-compiler can be found in $PATH (e.g
# gcc-arm-linux-gnueabihf is installed on Debian). Otherwise the paths should
# be adjusted


SYSROOT=$(dirname $(realpath $0))
GNU_TYPE=arm-linux-gnueabihf

cat << EOF

[binaries]
c = 'arm-linux-gnueabihf-gcc'
ar = 'arm-linux-gnueabihf-ar'
strip = 'arm-linux-gnueabihf-strip'
pkgconfig = '${SYSROOT}/pkg-config-sysroot'

[properties]
has_function_printf = true
root = '${SYSROOT}'
c_args = ['--sysroot=${SYSROOT}']
c_link_args = ['--sysroot=${SYSROOT}',
  '-L${SYSROOT}/lib',
  '-L${SYSROOT}/lib/${GNU_TYPE}',
  '-L${SYSROOT}/usr/lib',
  '-L${SYSROOT}/usr/lib/${GNU_TYPE}']

[host_machine]
system = 'linux'
cpu_family = 'arm'
cpu = 'armv7hl'
endian = 'little'
EOF
