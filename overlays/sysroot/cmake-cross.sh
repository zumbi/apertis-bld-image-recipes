#!/bin/sh
# Generate the content of a cmake-cross.txt file for usage to cross-compile
# against this chroot. Should be run once the chroot is in its final location
# to make sure all the patch are right

# Assumes that a cross-compiler can be found in $PATH (e.g
# gcc-arm-linux-gnueabihf is installed on Debian). Otherwise the paths should
# be adjusted


SYSROOT=$(dirname $(realpath $0))
GNU_TYPE=arm-linux-gnueabihf

cat << EOF
set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR arm)

set(CMAKE_SYSROOT ${SYSROOT})
set(CMAKE_STAGING_PREFIX ${SYSROOT}/usr/)

set(tools /usr)
set(CMAKE_C_COMPILER \${tools}/bin/arm-linux-gnueabihf-gcc)
set(CMAKE_CXX_COMPILER \${tools}/bin/arm-linux-gnueabihf-g++)

set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)
EOF
